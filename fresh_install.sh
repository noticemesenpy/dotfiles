#!/bin/bash
pacman -Syu --noconfirm
pacman -S --noconfirm clementine
pacman -S --noconfirm deluge
pacman -S --noconfirm htop
pacman -S --noconfirm screenfetch
pacman -S --noconfirm youtube-dl
pacman -S --noconfirm anki
pacman -S --noconfirm cmatrix
pacman -S --noconfirm calibre
pacman -S --noconfirm tmux
pacman -S --noconfirm tree
pacman -S --noconfirm redshift
pacman -S --noconfirm gedit
pacman -S --noconfirm thefuck
pacman -S --noconfirm chromium
pacman -S --noconfirm xfce4-screenshooter
pacman -S --noconfirm samba
pacman -S --noconfirm gufw
pacman -S --noconfirm cronie
pacman -S --noconfirm vim
pacman -S --noconfirm streamlink
pacman -S --noconfirm keepassxc

###-----AUR----###

# stockfish
# qtwebflix-git
# scid_vs_pc
# slack-desktop
# dropbox
