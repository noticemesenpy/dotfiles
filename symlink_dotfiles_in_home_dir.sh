#!/bin/bash
cp ~/.bashrc ~/.bashrc.BAK 2>/dev/null
rm ~/.bashrc 2>/dev/null
ln -s ~/dotfiles/bashrc ~/.bashrc

cp -r ~/.bin ~/.bin.BAK 2>/dev/null
rm -r ~/.bin 2>/dev/null
ln -s ~/dotfiles/bin/ ~/.bin

cp ~/.inputrc ~/.inputrc.BAK 2>/dev/null
rm ~/.inputrc 2>/dev/null
ln -s ~/dotfiles/inputrc ~/.inputrc

cp ~/.tmux.conf ~/.tmux.conf.BAK 2>/dev/null
rm ~/.tmux.conf 2>/dev/null
ln -s ~/dotfiles/tmux.conf ~/.tmux.conf

cp -r ~/.vim ~/.vim.BAK 2>/dev/null
rm -r ~/.vim 2>/dev/null
ln -s ~/dotfiles/vim ~/.vim

cp ~/.vimrc ~/.vimrc.BAK 2>/dev/null
rm ~/.vimrc 2>/dev/null
ln -s ~/.vim/vimrc ~/.vimrc

cp ~/.gitconfig ~/.gitconfig.BAK 2>/dev/null
rm ~/.gitconfig 2>/dev/null
ln -s ~/dotfiles/gitconfig ~/.gitconfig
